package com.example.myapplication;

public class Item {
    String title;
    String img_url;

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
