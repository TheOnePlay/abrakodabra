package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {

    String uri = "http://mskko2021.mad.hakta.pro/api/feelings";
    ArrayList<Item> arrayList;
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recycler);
        arrayList = new ArrayList<Item>();
        ApiAsync apiAsync = new ApiAsync();
        apiAsync.execute(uri);

    }

    public void setListItems() {
        Adapter adapter = new Adapter(this,arrayList);
        recyclerView.setAdapter(adapter);
    }

    class ApiAsync extends AsyncTask<String,String,String> {
        @Override
        protected String doInBackground(String... strings) {
            HttpURLConnection connection = null;
            BufferedReader bufferedReader = null;

            try {
                URL url =new URL(strings[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream inputStream = connection.getInputStream();
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                StringBuffer stringBuffer = new StringBuffer();
                String line = "";

                while((line=bufferedReader.readLine())!= null)
                    stringBuffer.append(line).append("\n");
                return stringBuffer.toString();
            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i = 0; i< jsonArray.length(); i++) {
                    String title = (String) jsonArray.getJSONObject(i).get("title");
                    String img_url = (String) jsonArray.getJSONObject(i).get("image");

                    Item newItem = new Item();
                    newItem.setImg_url(img_url);
                    newItem.setTitle(title);

                    arrayList.add(newItem);

                }
                setListItems();
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }
    }

}